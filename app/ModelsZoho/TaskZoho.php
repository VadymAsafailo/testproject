<?php

namespace App\ModelsZoho;

use App\Models\Deal;
use App\Models\Task;
use ZohoCrmSDK\ModelsZoho\TaskZohoModel;

class TaskZoho extends TaskZohoModel
{
    protected $modelDB = Task::class;
//    protected $received = ['id', 'Subject'];
    public function relatedContacts()
    {
        return $this->hasList(ContactZoho::class,'Contacts');
    }

    public function relatedDeals()
    {
        return $this->hasList(DealZoho::class,'Deals');
    }
}
