<?php

namespace App\ModelsZoho;

use App\Models\Test;
use ZohoCrmSDK\ModelsZoho\TestZohoModel;

class TestZoho extends TestZohoModel
{
    protected $modelDB = Test::class;
}
