<?php

namespace App\ModelsZoho;

use App\Models\Deal;
use ZohoCrmSDK\ModelsZoho\DealZohoModel;

class DealZoho extends DealZohoModel
{
    protected $modelDB = Deal::class;

    protected $lookupNamesReceived=['Stage'=>'stage_names'];
    protected $relatedLists = ["tasks"];

    public function relatedTasks()
    {
        return $this->hasList(TaskZoho::class, 'tasks');
    }


}
