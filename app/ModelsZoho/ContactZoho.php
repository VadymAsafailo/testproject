<?php

namespace App\ModelsZoho;

use App\Models\Birthday;
use App\Models\Contact;
use App\Models\Test;
use ZohoCrmSDK\ModelsZoho\ContactZohoModel;

class ContactZoho extends ContactZohoModel
{
    protected $modelDB = Contact::class;


    protected $relatedLists = ["Tasks"];

    public function relatedTasks()
    {
        return $this->hasList(TaskZoho::class, 'Tasks');
    }

    protected $subforms = [
        'Subform1' => [
            'modelDB' => Test::class,
            'name' => 'Subform1',
            'renamed' => [
                'name1' => 'name1',
                'birthday' => 'birthday'
            ]
        ],
        'Subform2' => [
            'modelDB' => Birthday::class,
            'name' => 'Subform2',
            'renamed' => [
                'gender' => 'gender',
                'birthday' => 'birthday'
            ]
        ]
    ];
//
//    protected $received = ['id', 'Account_Name', 'Owner'];
//
////    protected $ignored =['Account_Name','Owner'];
////    protected $relationLists = ['deals'];
//    protected $renamed = [
//        'Account_Name' => 'Account_Id',
//        'Owner' => 'Owner_Id'
//    ];
//
//    protected $softDelete = true;
//    protected $lookupNamesReceived = ['Account_Name', 'Owner'];

}
