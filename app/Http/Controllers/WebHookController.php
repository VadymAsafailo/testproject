<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebHookController extends Controller
{
    public function dataReceipt(Request $request)
    {
        Log::info($request);
    }
}
