<?php

namespace App\Http\Controllers;

use App\ModelsZoho\ContactZoho;
use Illuminate\Http\Request;
use ZohoCrmSDK\Api\ZohoCrmApi;

class TestController extends Controller
{
    public function test()
    {

        $dataRecords = [
            'Last_Name' => 'from sdk'
        ];
        $checkFields = ['Last_Name'];

        $checkFields =
        $record = ZohoCrmApi::getInstance()
            ->setModule('Contacts')
            ->records()
            ->upsertRecords($dataRecords, $checkFields)
            ->request();
    }

    public function listOfModules()
    {
        $resp = ZohoCrmApi::getInstance()->metaData()->modules()->request();
        dd($resp);
    }

    public function relatedLists()
    {
//        $resp = ZohoCrmApi::getInstance()
//            ->setModule('Contacts')
//            ->metaData()
//            ->relatedLists()
//            ->request();
//        dd($resp);
        $resp = ZohoCrmApi::getInstance()
            ->setModule('Contacts')
            ->records()
            ->getRelatedRecords()
            ->request();
return $resp;
    }
}
