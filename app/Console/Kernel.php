<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('zoho-crm-sdk:sync-records ContactZoho --interval=2 --sub-type=Hours')->hourly();
        $schedule->command('zoho-crm-sdk:sync-records DealZoho --interval=2 --sub-type=Hours')->hourly();
        $schedule->command('zoho-crm-sdk:sync-trashed DealZoho --interval=3 --sub-type=Hours')->everyTwoHours();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
