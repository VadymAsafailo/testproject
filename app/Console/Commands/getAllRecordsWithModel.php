<?php

namespace App\Console\Commands;

use App\ModelsZoho\TaskZoho;
use Illuminate\Console\Command;

class getAllRecordsWithModel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'returnAllTaskRecords';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $records = [];


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle($page=1)
    {
        if (is_null(TaskZoho::all($page, true, 200))) {
            dd($this->records);
        } else {
            $recordsAtCurrentPage = TaskZoho::all($page, true, 200);;
            $this->records = array_merge($this->records, $recordsAtCurrentPage);
            $this->handle(++$page);
        }
    }
}
