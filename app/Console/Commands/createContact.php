<?php

namespace App\Console\Commands;

use App\ModelsZoho\ContactZoho;
use Illuminate\Console\Command;

class createContact extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createContact';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $records = ContactZoho::all(
        );
        return $records;
    }
}
