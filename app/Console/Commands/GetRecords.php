<?php

namespace App\Console\Commands;

use App\ModelsZoho\TaskZoho;
use Illuminate\Console\Command;

class GetRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getRecords';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tasks = TaskZoho::all(1,true);
        dd($tasks);

    }
}
