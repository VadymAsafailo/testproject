<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use ZohoCrmSDK\Api\ZohoCrmApi;

class InsertRecord extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insertRecord';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $records = [];


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $newContact = ZohoCrmApi::getInstance()
            ->records()
            ->setModule('Contacts')
            ->insertRecords([
                [
                    'Last_Name' => 'FromSDK1%' . rand(0, 1000),
                ]
            ])
            ->request();
        $newContactId = $newContact[0];

        $newDeal = ZohoCrmApi::getInstance()
            ->records()
            ->setModule('Deals')
            ->insertRecords([
                [
                    'Deal_Name' => "Deal#" . rand(0, 1000),
                    "Account_Name" => 594155000000402292,
                    'Closing_Date' => '30.06.2023',
                    'Stage' => 'Qualification'
                ]
            ])
            ->request();
        $dealId = $newDeal[0];

        $newTask = ZohoCrmApi::getInstance()
            ->records()
            ->setModule('Tasks')
            ->insertRecords([
                [
                    'Subject' => "test Subject#" . rand(1, 1000),
                ]
            ])
            ->request();

        dd($newTask);


    }
}
