<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use ZohoCrmSDK\Api\ZohoCrmApi;

class DeleteYesterdayRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deleteY:module {module} {days}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Description Delete records from particular
     module(first argument), created within defined number of days(second argument)";

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $module = $this->argument('module');
        $days = $this->argument('days');
        $yesterdayRecords = ZohoCrmApi::getInstance()
            ->setModule($module)
            ->records()
            ->getRecords()
            ->modifiedAfter(now()->subDays($days))
            ->request();


        $yesterdayRecords = ZohoCrmApi::getInstance()
            ->setModule($module)
            ->records()
            ->queryCOQL()
            ->columns(['id'])
            ->whereSearchMap([
                ['Created_Time', '>=', Carbon::now()->subDays(1)->toAtomString()],
            ])
            ->request();
        $listIds = collect($yesterdayRecords)->pluck('id')->toArray();

        $deleteResponse = ZohoCrmApi::getInstance()
            ->setModule($module)
            ->records()
            ->deleteRecords($listIds)
            ->request();

        dd($deleteResponse);
    }
}
