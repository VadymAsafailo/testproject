<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use ZohoCrmSDK\Api\Exceptions\NoContentException;
use ZohoCrmSDK\Api\Exceptions\NoModifiedException;
use ZohoCrmSDK\Api\ZohoCrmApi;

class AllRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AllRecords';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $records = [];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->getAll();
        dd(count($this->records));
    }

    public function getAll($page = 1)
    {
        dump('Page=' . $page);
        try {
            $resp = ZohoCrmApi::getInstance()
                ->setModule('Tasks')
                ->records()
                ->getRecords()
                ->page($page)
                ->request();
            $this->records = array_merge($this->records, $resp);
        } catch (\Exception $exception) {
            if ($exception instanceof NoModifiedException ||
                $exception instanceof NoContentException) {

                return;
            }
            throw $exception;
        }
        $this->getAll(++$page);
    }
}
