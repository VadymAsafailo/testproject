<?php

namespace App\Console\Commands;

use App\ModelsZoho\ContactZoho;
use Illuminate\Console\Command;
use ZohoCrmSDK\ModelsZoho\AccountZohoModel;
use ZohoCrmSDK\ModelsZoho\ContactZohoModel;

class ModelTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ModelTest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $contacts = ContactZoho::all(1,true);
        dd($contacts);
    }
}
