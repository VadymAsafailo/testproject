<?php

namespace App\Console\Commands;

use App\ModelsZoho\TaskZoho;
use Illuminate\Console\Command;

class createTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CreateTask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $task = TaskZoho::new([
            'subject'=>'New'
        ]);



        $task->saveToZoho();
        $task->saveToDB();
    }
}
