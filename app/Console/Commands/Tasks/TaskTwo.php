<?php

namespace App\Console\Commands\Tasks;

use App\ModelsZoho\ContactZoho;
use Faker\Factory;
use Illuminate\Console\Command;

class TaskTwo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TaskTwo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $faker = Factory::create();
        $contact = ContactZoho::new([
            'last_name' => 'TaskTwo' . $faker->name . rand(1, 1000)
        ]);
        $contact->saveToZoho();
        $contactId = $contact->id;

        $subform = $contact->addRowSubform('Subform1',
            ['name1' => 'TestName' . rand(1, 10000), 'birthday' => "27.06.2023"]);
        $subform = $contact->addRowSubform('Subform1',
            ['name1' => 'TestName' . rand(1, 10000), 'birthday' => "27.06.2023"]);

        $contact->saveToZoho();

        $contact = ContactZoho::find($contactId);
        $subformRows = $contact->getRowsSubform('Subform1');

        foreach ($subformRows as $row) {
            $rowData = ['id' => $row['id'], 'name1' => 'TestingName' .
                rand(1, 10000), 'birthday' => "27.06.1992"];
            $subformUpdate = $contact->addRowSubform('Subform1', $rowData);
            $contact->saveToZoho();
            $rowToDeleteId = $row['id'];
        }
        unset($subformRows[$rowToDeleteId]);
        //Deletion of subform row


        $deleteSubformRow = $contact->setRowsSubform('Subform1', $subformRows);
        $contact->saveToZoho();


    }
}
