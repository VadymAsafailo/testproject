<?php

namespace App\Console\Commands\Tasks;

use App\ModelsZoho\ContactZoho;
use Carbon\Carbon;
use Illuminate\Console\Command;

class TaskFour extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TaskFour {date1} {date2}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dateFrom = $this->arguments()['date1'];
        $dateTo = $this->arguments()['date2'];


        $contacts = ContactZoho::query()
            ->columns(['id', 'first_name', 'last_name', 'email'])
            ->whereSearchMap([
                ['created_time', '>=', Carbon::create($dateFrom)->toAtomString()],
                'and',
                ['created_time', '<=', Carbon::create($dateTo)->endOfDay()->toAtomString()]
            ])
            ->page(1)
            ->perPage(3)
            ->get();
        dd($contacts);
    }
}
