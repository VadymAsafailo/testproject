<?php

namespace App\Console\Commands\Tasks;

use Carbon\Carbon;
use Illuminate\Console\Command;
use ZohoCrmSDK\Api\Exceptions\NoContentException;
use ZohoCrmSDK\Api\Exceptions\NoModifiedException;
use ZohoCrmSDK\Api\ZohoCrmApi;

class TaskThree extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TaskThree {time}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $records = [];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle($page = 1)
    {

        echo 'current page= ' . $page;
        $time = $this->argument('time');

        try {
            $accounts = ZohoCrmApi::getInstance()
                ->setModule('Accounts')
                ->records()
                ->queryCOQL()
                ->columns(['Account_Name', 'Website', 'Created_Time'])
                ->whereSearchMap([
                    ['Created_Time', '>=', Carbon::create($time)->toAtomString()],
                    'and',
                    ['Created_Time', '<=', Carbon::create($time)->endOfDay()->toAtomString()]
                ])
                ->page($page)
                ->perPage(1)
                ->request();
            $this->records = array_merge($this->records, $accounts);

        } catch (\Exception $exception) {
            if ($exception instanceof NoModifiedException ||
                $exception instanceof NoContentException) {

                dd($this->records);
            }
            throw $exception;
        }
        $this->handle(++$page);

    }
}
