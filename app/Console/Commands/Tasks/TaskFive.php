<?php

namespace App\Console\Commands\Tasks;

use App\ModelsZoho\AccountZoho;
use Illuminate\Console\Command;

class TaskFive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TaskFive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $account = AccountZoho::new([
            'account_name' => 'Test' . rand(1, 10000)
        ]);
        $account->saveToZoho();
        $accountId = $account->id;
        $attachments = AccountZoho::attachmentsUpload($accountId, storage_path('app\file\Untitled.jpg'));

        $attachments = AccountZoho::attachmentsAll($accountId);
        $content = $attachments->downloadByIndex(0);
        file_put_contents(storage_path('app\file\attach' . rand(1, 1000)).'.jpg', $content);
        dd($content);


        dd($attachments);





    }
}
