<?php

namespace App\Console\Commands\Tasks;

use App\ModelsZoho\ContactZoho;
use Faker\Factory;
use Illuminate\Console\Command;

class TaskSix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TaskSix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $faker = Factory::create();
        $contact = ContactZoho::new([
            'last_name' => 'TaskTwo' . $faker->name . rand(1, 1000)
        ]);
        $rows = [['gender' => 'male', 'birthday' => "27.06.2023"],
            ['gender' => 'female', 'birthday' => "27.06.2023"]];

        $contact->addRowsSubform('Subform2', $rows);

        $contact->saveToZoho(['workflow']);
        $contact = ContactZoho::find($contact->id);
        $contact->saveToDB();
        $contact->saveSubformsToDB();

    }
}
