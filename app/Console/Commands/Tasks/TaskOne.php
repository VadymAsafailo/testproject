<?php

namespace App\Console\Commands\Tasks;

use App\ModelsZoho\AccountZoho;
use App\ModelsZoho\ContactZoho;
use App\ModelsZoho\DealZoho;
use App\ModelsZoho\TaskZoho;
use Faker\Factory;
use Illuminate\Console\Command;
use ZohoCrmSDK\Api\Exceptions\NoContentException;

class TaskOne extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TaskOne';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $faker;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $faker = Factory::create();
        $contact = ContactZoho::new([
            'last_name' => 'NewContact' . $faker->name . rand(1, 1000)
        ]);
        $contact->saveToZoho();
        $contact->saveToDB();

        $accounts = AccountZoho::all();
        $accountsKeys = collect($accounts)->keys()->toArray();

        $deal = DealZoho::new([
            'deal_name' => 'NewDeal' . '|' . $faker->name . '|' . rand(1, 1000),
            'stage' => 'Qualification',
            'closing_date' => $faker->date,
            'account_name' => $accountsKeys[rand(1, count($accountsKeys) - 1)]
        ]);
        $deal->saveToZoho();
        $deal->saveToDB();


        $task = TaskZoho::new([
            'subject' => "newSubject" . $faker->name,
            'se_module' => 'Deals',
            'what_id' => $deal->id,
            'who_id' => $contact->id,
        ]);
        $task->saveToZoho();
        $task->saveToDB();

        try {

            $data = [[
                ['id' => $deal->id]
            ]];


        } catch (\Exception $exception) {
            if ($exception instanceof NoContentException) {
                dd($exception->getMessage());
            }
        }

    }
}
