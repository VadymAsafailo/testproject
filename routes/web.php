<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', [\App\Http\Controllers\TestController::class, 'test'])->name('test');
Route::get('/listModules', [\App\Http\Controllers\TestController::class, 'listOfModules'])->name('listOfModules');
Route::get('/relatedLists', [\App\Http\Controllers\TestController::class, 'relatedLists'])->name('relatedLists');
